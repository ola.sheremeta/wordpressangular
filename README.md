# About Blog Project

This project was generated with [Angular CLI](https://imgur.com/a/w65OZT1) version 11.0.5.
<br>
<br>
Simple useful project. Combined two popular CMS & framework. Wordpress on back-end and Angular on frontend.
<br>
<br>
**Implemented Functionality**
1. **Output:** posts, comments, authors, categories and tags.
2. **Sorting posts:** by categories, by tags, by authors.
3. CRUD operation: post comments unregistered users with postmoderation.
4. **Output Global Wordpress Search.**
5. Pages Lazy-loading.
6. SEO optimization and pre-render Angular Universal.
7. **CSS layout:** Flexbox, mobile responsive. 
<br>

**Some project foto and video:**

![alt tag](https://i.imgur.com/3c7Mveo.jpg "Blog Foto")


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
